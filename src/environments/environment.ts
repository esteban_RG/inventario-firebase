// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: 'AIzaSyBR0-GxWvGI8fMFP-ffknvUf0bdt6JxkGo',
  authDomain: 'inventario-635ff.firebaseapp.com',
  databaseURL: 'https://inventario-635ff.firebaseio.com',
  projectId: 'inventario-635ff',
  storageBucket: 'inventario-635ff.appspot.com',
  messagingSenderId: '202399268595',
  appId: '1:202399268595:web:349ecaa764608aa0510186',
  measurementId: 'G-799SNRZRP6'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
