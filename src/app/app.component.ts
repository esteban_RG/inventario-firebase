import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'inventario Marsella';

  public activado: boolean;
  // tslint:disable-next-line: use-lifecycle-interface
  ngOnInit(): void {
    this.activado = false;
  }

  public onRouterOutletActivate(event: any) {
    const datos = localStorage.getItem('token');
    if (event && datos) {
      this.activado = true;
    } else {
      this.activado = false;
    }
  }

}
