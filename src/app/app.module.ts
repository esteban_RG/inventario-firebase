
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { AppComponent } from './app.component';

import { APP_ROUTING } from './app.routes';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { InsideComponent } from './components/components/inside.component';
import { HeaderComponent } from './components/components/header/header.component';
import { FooterComponent } from './components/components/footer/footer.component';
import { InsideModule } from './components/components/inside.module';
import { OlvidePasswordComponent } from './pages/olvide-password/olvide-password.component';
import { PageNotFoundComponentComponent } from './pages/page-not-found-component/page-not-found-component.component';
import { MaterialModuleModule } from './material-module.module';
import { firebaseConfig } from 'src/environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    InsideComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    OlvidePasswordComponent,
    PageNotFoundComponentComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    HttpClientModule,
    RouterModule,
    InsideModule,
    DataTablesModule,
    MaterialModuleModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    APP_ROUTING,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    RouterModule,
    MaterialModuleModule,
  ],
  providers: [
    { provide: StorageBucket, useValue: '//inventario-635ff.appspot.com/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
