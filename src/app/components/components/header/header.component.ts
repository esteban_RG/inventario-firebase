import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor( private login: LoginService, private roter: Router) { }

  ngOnInit() {
  }

  cerrarSesion() {
    this.login.logOut();
  }
}
