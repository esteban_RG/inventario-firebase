import { NgModule } from '@angular/core';

// design
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { from } from 'rxjs';
import { MaterialModuleModule } from 'src/app/material-module.module';

// routes
import { RouterModule } from '@angular/router';
import { APP_ROUTING } from './inside.routes';

import { DataTablesModule } from 'angular-datatables';

// firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { firebaseConfig } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage';

// modulos
import { ActivosComponent } from './activos/activos/activos.component';
import { DadosDeBajaComponent } from './activos/dados-de-baja/dados-de-baja.component';
import { TransferirActivoComponent } from './activos/transferir-activo/transferir-activo.component';
import { LineaComponent } from './activos/linea/linea.component';
import { SublineaMuebleComponent } from './activos/sublinea-mueble/sublinea-mueble.component';
import { SublineaInmuebleComponent } from './activos/sublinea-inmueble/sublinea-inmueble.component';
import { AreaComponent } from './area/area.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { ReporteActivosComponent } from './reportes/reporte-activos/reporte-activos.component';
import { ReporteAreaComponent } from './reportes/reporte-area/reporte-area.component';
import { ReporteFuncionariosComponent } from './reportes/reporte-funcionarios/reporte-funcionarios.component';
import { ReporteUsuariosComponent } from './reportes/reporte-usuarios/reporte-usuarios.component';
import { CargoUsuarioComponent } from './usuarios/cargo-usuario/cargo-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios/usuarios.component';
import { PermisosComponent } from './usuarios/permisos/permisos.component';

// modales
import { EditarActivosComponent } from '../modals/editar-activos/editar-activos.component';

@NgModule({
  declarations: [
    ActivosComponent,
    DadosDeBajaComponent,
    TransferirActivoComponent,
    LineaComponent,
    SublineaMuebleComponent,
    SublineaInmuebleComponent,
    AreaComponent,
    FuncionariosComponent,
    ReporteActivosComponent,
    ReporteAreaComponent,
    ReporteFuncionariosComponent,
    ReporteUsuariosComponent,
    CargoUsuarioComponent,
    UsuariosComponent,
    PermisosComponent,
    EditarActivosComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    RouterModule,
    DataTablesModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireStorageModule,
    MaterialModuleModule,
    AngularFireAuthModule,
    APP_ROUTING
  ],
  providers: [
    { provide: StorageBucket, useValue: '//inventario-635ff.appspot.com/'}
  ],
  entryComponents: [
    EditarActivosComponent
  ],
  exports: [
    RouterModule,
    MaterialModuleModule,
   ]
})
export class InsideModule { }
