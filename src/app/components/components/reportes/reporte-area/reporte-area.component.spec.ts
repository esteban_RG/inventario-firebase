import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteAreaComponent } from './reporte-area.component';

describe('ReporteAreaComponent', () => {
  let component: ReporteAreaComponent;
  let fixture: ComponentFixture<ReporteAreaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteAreaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
