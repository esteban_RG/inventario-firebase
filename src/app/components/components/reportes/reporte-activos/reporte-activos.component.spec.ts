import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteActivosComponent } from './reporte-activos.component';

describe('ReporteActivosComponent', () => {
  let component: ReporteActivosComponent;
  let fixture: ComponentFixture<ReporteActivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteActivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteActivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
