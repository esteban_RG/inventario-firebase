import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReporteFuncionariosComponent } from './reporte-funcionarios.component';

describe('ReporteFuncionariosComponent', () => {
  let component: ReporteFuncionariosComponent;
  let fixture: ComponentFixture<ReporteFuncionariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReporteFuncionariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReporteFuncionariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
