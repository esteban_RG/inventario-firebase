import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ActivosComponent } from './activos/activos/activos.component';
import { DadosDeBajaComponent } from './activos/dados-de-baja/dados-de-baja.component';
import { TransferirActivoComponent } from './activos/transferir-activo/transferir-activo.component';
import { LineaComponent } from './activos/linea/linea.component';
import { SublineaMuebleComponent } from './activos/sublinea-mueble/sublinea-mueble.component';
import { SublineaInmuebleComponent } from './activos/sublinea-inmueble/sublinea-inmueble.component';
import { AreaComponent } from './area/area.component';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { ReporteActivosComponent } from './reportes/reporte-activos/reporte-activos.component';
import { ReporteAreaComponent } from './reportes/reporte-area/reporte-area.component';
import { ReporteFuncionariosComponent } from './reportes/reporte-funcionarios/reporte-funcionarios.component';
import { ReporteUsuariosComponent } from './reportes/reporte-usuarios/reporte-usuarios.component';
import { CargoUsuarioComponent } from './usuarios/cargo-usuario/cargo-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios/usuarios.component';
import { PermisosComponent } from './usuarios/permisos/permisos.component';
import { InsideComponent } from './inside.component';
import { AuthGuard } from 'src/app/guards/auth.guard';

const routes: Routes = [
  { path: 'inside', component: InsideComponent, canActivate: [AuthGuard], children: [
    { path: 'activos', component: ActivosComponent},
    { path: 'debaja', component: DadosDeBajaComponent },
    { path: 'transferirActivo', component: TransferirActivoComponent },
    { path: 'linea', component: LineaComponent },
    { path: 'sublineaMueble', component: SublineaMuebleComponent },
    { path: 'sublineaInmueble', component: SublineaInmuebleComponent },
    { path: 'area', component: AreaComponent },
    { path: 'funcionarios', component: FuncionariosComponent },
    { path: 'reporteActivos', component: ReporteActivosComponent },
    { path: 'reporteArea', component: ReporteAreaComponent },
    { path: 'reporteFuncionarios', component: ReporteFuncionariosComponent },
    { path: 'ReporteUsuarios', component: ReporteUsuariosComponent },
    { path: 'cargoUsuario', component: CargoUsuarioComponent },
    { path: 'usuarios', component: UsuariosComponent },
    { path: 'permisos', component: PermisosComponent },
    { path: '', redirectTo: '/inside/activos', pathMatch: 'full' }
  ]},
];

export const APP_ROUTING = RouterModule.forChild(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeatureRoutingModule {}
