import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosDeBajaComponent } from './dados-de-baja.component';

describe('DadosDeBajaComponent', () => {
  let component: DadosDeBajaComponent;
  let fixture: ComponentFixture<DadosDeBajaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DadosDeBajaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosDeBajaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
