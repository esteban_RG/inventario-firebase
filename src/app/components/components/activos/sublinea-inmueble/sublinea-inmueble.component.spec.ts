import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SublineaInmuebleComponent } from './sublinea-inmueble.component';

describe('SublineaInmuebleComponent', () => {
  let component: SublineaInmuebleComponent;
  let fixture: ComponentFixture<SublineaInmuebleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SublineaInmuebleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SublineaInmuebleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
