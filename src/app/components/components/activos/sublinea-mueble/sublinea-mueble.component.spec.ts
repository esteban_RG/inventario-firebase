import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SublineaMuebleComponent } from './sublinea-mueble.component';

describe('SublineaMuebleComponent', () => {
  let component: SublineaMuebleComponent;
  let fixture: ComponentFixture<SublineaMuebleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SublineaMuebleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SublineaMuebleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
