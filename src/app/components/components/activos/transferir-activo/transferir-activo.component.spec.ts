import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferirActivoComponent } from './transferir-activo.component';

describe('TransferirActivoComponent', () => {
  let component: TransferirActivoComponent;
  let fixture: ComponentFixture<TransferirActivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransferirActivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferirActivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
