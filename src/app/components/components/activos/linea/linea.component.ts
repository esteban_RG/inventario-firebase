import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { LineaService } from '../../../../services/linea.service';
import { Linea } from '../../../../models/linea';
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import Swal from 'sweetalert2';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { EditarActivosComponent } from 'src/app/components/modals/editar-activos/editar-activos.component';

@Component({
  selector: 'app-linea',
  templateUrl: './linea.component.html',
  styleUrls: ['./linea.component.scss']
})

export class LineaComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  public linea$: Observable<Linea[]>;
  displayedColumns: string[] = ['id', 'ValorActivo', 'tipoActivo', 'Actions'];
  // tslint:disable-next-line: no-use-before-declare
  dataSource = new MatTableDataSource();

  constructor(private services: LineaService, public dialog: MatDialog) { }

  ngOnInit() {
   // this.services.getAllPost().subscribe( (data) => { console.log(data)} );
    this.services.getAllPost().subscribe( result => (this.dataSource.data = result));
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  create() {}

  edit(dta: Linea) {
    console.log(dta);
    this.dialog.open(EditarActivosComponent, {
      width: '35%',
      data: dta
    });

  }

  delete(data: Linea) {
    console.log(data);
    Swal.fire({
      title: 'Eliminar',
      text: 'Desea eliminar este elemento',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'si, eliminar'
    }).then( result => {
      if (result.value) {
        this.services.deleteLineById(data).then( () => {
          Swal.fire('Eliminado', 'la linea se elimino', 'success');
        }).catch((error) => {
          Swal.fire('Error', 'error al eliminar' + error, 'error');
        });
      }
    });
  }

}
