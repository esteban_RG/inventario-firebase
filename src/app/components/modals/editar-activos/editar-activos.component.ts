import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Linea } from '../../../models/linea';
import { NgForm } from '@angular/forms';
import { LineaService } from '../../../services/linea.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-activos',
  templateUrl: './editar-activos.component.html',
  styleUrls: ['./editar-activos.component.scss']
})
export class EditarActivosComponent implements OnInit {

  // tslint:disable-next-line: new-parens
  public datos: Linea = new Linea;
  constructor(@Inject(MAT_DIALOG_DATA) public data: Linea, private service: LineaService) { }

  ngOnInit() {
    console.log(this.data);
    this.valorInicial();
  }

  editar(form: NgForm) {
    this.service.editLinea(this.datos).then(result => {
      console.log(result);
    }).catch(error => {
      console.log(error);
    });
  }
  valorInicial() {
    this.datos.ValorActivo = this.data.ValorActivo;
    this.datos.id = this.data.id;
    this.datos.tipoActivo = this.data.tipoActivo;
  }
}
