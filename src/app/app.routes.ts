import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './pages/login/login.component';
import { InsideComponent } from './components/components/inside.component';
import { AuthGuard } from './guards/auth.guard';
import { OlvidePasswordComponent } from './pages/olvide-password/olvide-password.component';
import { PageNotFoundComponentComponent } from './pages/page-not-found-component/page-not-found-component.component';


const routes: Routes = [
   { path: 'login', component: LoginComponent},
   { path: 'inside', component: InsideComponent, canActivate: [AuthGuard]},
   { path: 'olvidopass', component: OlvidePasswordComponent},
   { path: '', redirectTo: '/login', pathMatch: 'full' },
   { path: '**', component: PageNotFoundComponentComponent  }
];
// tslint:disable-next-line: variable-name
export const APP_ROUTING = RouterModule.forRoot(routes);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class FeatureRoutingModule {}
