import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { loginModel } from '../models/login.model';
import { map } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { error } from 'util';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public userData: Observable<firebase.User>;
  public token;

  constructor(private auth: AngularFireAuth, private router: Router) {
    this.userData = this.auth.authState;
    this.leerToken();
  }

  login(user: loginModel) {
    const {email, password} = user;
    return this.auth.auth.signInWithEmailAndPassword(email, password).then( result => {
      // tslint:disable-next-line: no-shadowed-variable
      result.user.getIdTokenResult().then( result => {
        localStorage.setItem('alpha', result.issuedAtTime.toString());
        localStorage.setItem('omega', result.expirationTime.toString());
        localStorage.setItem('token', result.token);
        this.leerToken();
      });
    });
  }

  leerToken() {

    if (localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');
    } else {
      this.token = '';
    }
  }

  logOut() {
    this.auth.auth.signOut().then(() => {
      localStorage.removeItem('token');
      localStorage.removeItem('omega');
      localStorage.removeItem('alpha');
      this.router.navigateByUrl('/login');
   });
  }

  estaAutenticado(): boolean {

    const expira = moment(localStorage.getItem('omega'));
    const ini = moment(localStorage.getItem('alpha'));

    if (!this.token) {
      return false;
    }

    if ( ini >= expira) {
      this.logOut();
      return false;
    } else {
      return true;
    }
  }

}
