import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Linea } from '../models/linea';

@Injectable({
  providedIn: 'root'
})
export class LineaService {

  private lineaCollections: AngularFirestoreCollection<Linea>;

  constructor(private afs: AngularFirestore) {
    this.lineaCollections = afs.collection<Linea>('linea');
  }

  public getAllPost(): Observable<Linea[]> {
    return this.lineaCollections.snapshotChanges().pipe(
      map(actions =>
        actions.map(a => {
        const data = a.payload.doc.data() as Linea ;
        const id = a.payload.doc.id;
        return { id, ...data};
      }))
    );
  }

  deleteLineById(linea: Linea) {
    return this.lineaCollections.doc(linea.id).delete();
  }

  editLinea(linea: Linea) {
    const line: Linea = {
      id: linea.id,
      ValorActivo: linea.ValorActivo,
      tipoActivo: linea.tipoActivo
    };
    return this.lineaCollections.doc(line.id).update(line);
  }

  addLinea(linea: Linea) {
    const line: Linea = {
      id: linea.id,
      ValorActivo: linea.ValorActivo,
      tipoActivo: linea.tipoActivo
    };
    this.lineaCollections.add(line);
  }
}
