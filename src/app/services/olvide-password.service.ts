import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class OlvidePasswordService {

  constructor(private auth: AngularFireAuth) { }

  resetPassword(email: any) {
    return this.auth.auth.sendPasswordResetEmail(email);
  }
}
