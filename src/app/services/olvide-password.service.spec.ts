import { TestBed } from '@angular/core/testing';

import { OlvidePasswordService } from './olvide-password.service';

describe('OlvidePasswordService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OlvidePasswordService = TestBed.get(OlvidePasswordService);
    expect(service).toBeTruthy();
  });
});
