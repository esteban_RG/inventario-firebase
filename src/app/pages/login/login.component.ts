import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { loginModel } from 'src/app/models/login.model';
import { LoginService } from '../../services/login.service';
import { error } from 'util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // tslint:disable-next-line: new-parens
  usuario: loginModel = new loginModel;
  recordarUsuario = false;

  constructor(private service: LoginService, private router: Router) { }

  ngOnInit() {
    localStorage.removeItem('token');
    localStorage.removeItem('omega');
    localStorage.removeItem('alpha');
  }

  login( form: NgForm) {
    if (form.invalid) {
      return;
    }

    Swal.fire({
      allowOutsideClick: false,
      text: 'espere por favor',
      icon: 'info',
    });
    Swal.showLoading();

    this.service.login(this.usuario).then(result => {
      Swal.close();
      this.router.navigateByUrl('/inside');
    }).catch( err => {
      console.log(err);
      Swal.fire({
        title: 'Error al autenticar',
        text: err.message,
        icon: 'warning',
      });
    });
  }

  olvidePass() {
    this.router.navigateByUrl('/olvidopass');
  }
}
