import { Component, OnInit } from '@angular/core';
import { OlvidePasswordService } from '../../services/olvide-password.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-olvide-password',
  templateUrl: './olvide-password.component.html',
  styleUrls: ['./olvide-password.component.scss']
})
export class OlvidePasswordComponent implements OnInit {

  public email: string;

  constructor(private service: OlvidePasswordService, private router: Router) { }

  ngOnInit() {
  }

  volver() {
    this.router.navigateByUrl('/login');
  }

  resetPass(email) {
    const re = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$');
    if (re.exec(email)) {
      this.service.resetPassword(email).then(result => {
        Swal.fire({
          allowOutsideClick: false,
          text: '' + result + '',
          icon: 'success',
        });
        this.router.navigateByUrl('/login');
      }).catch(error => {
        Swal.fire({
          allowOutsideClick: false,
          text: '"' + error + '"',
          icon: 'error',
        });
      });
    } else {
      Swal.fire({
        allowOutsideClick: false,
        text: 'Ingrese el campo email correctamente ' + '"' + email + '"',
        icon: 'error',
      });
      this.email = '';
    }
  }
}
